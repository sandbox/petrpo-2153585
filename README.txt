
ABOUT TWIGONDON
-----------

The Twigondon is a theme under new Twig template for Drupal 8.x


ABOUT DRUPAL THEMING
--------------------

It should follow Drupal and Twig standards:
http://twig.sensiolabs.org/doc/coding_standards.html
https://drupal.org/node/1823416
